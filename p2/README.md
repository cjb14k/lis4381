# LIS4381 - Mobile Web Application Development

## Carter Bass

### Project 2 Requirements:

*Three Parts:*

1. Create Web App that accesses a database to add, delete and edit records.
2. Screenshots of P2 main page including Data Table and error page when data is entered incorrectly, RSS feed.
3. Chapter Questions (Ch. 13, 14)

#### README.md file should include the following items:

* Screenshot of P2 Home;
* Screenshot of edit_petstore;
* Screenshot of edit_petstore_process with error;
* Screenshot of RSS Feed
* Link to local lis4381 web app;

> Assignment screenshot will appear below.
> 
> Five screenshots are shown in this README file.
>

#### Assignment Screenshot:

*Screenshot of Data Table*:

![P2 Data Table](img/capture1.PNG)

*Screenshot of edit_petstore*

![edit_petstore](img/capture2.PNG)

*Screenshot of edit_petstore_process*

![edit_petstore_process](img/capture3.PNG)

*Screenshot of Home Page*

![Home Page](img/capture4.PNG)

*Screenshot of RSS Feed*

![RSS Feed](img/capture5.PNG)

*Link to Local Web App*:

[Local Web App](http://localhost/lis4381/index.php)
