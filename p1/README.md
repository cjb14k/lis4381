# LIS4381 - Mobile Web Application Development

## Carter Bass

### Project 1 Requirements:

*Seven Parts:*

1. Backward-Engineer the screenshots to make a Business Card application with Name, Photo, Contact information and interests.
2. Create a launcher icon image and display it in both activities.
3. Must add background color to both activities.
4. Must add border around image and button.
5. Must add text shadow around button.
6. Screenshots of running applications first and second interface.
7. Chapter Questions (Ch. 7 & 8)

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application's first user interface;
* Screenshot of running application's second user interface;

> Assignment screenshot will appear below.
> 
> Two screenshots are shown in this README file.
>

#### Assignment Screenshot:

*Screenshot of first user interface*:

![First User Interface](img/img1.PNG)

*Screenshot of second user interface*:

![Second User Interface](img/img2.PNG)
