# LIS4381 - Mobile Web Application Development

## Carter Bass

### Assignment 5 Requirements:

*Three Parts:*

1. Create Web App that accesses a database to add, delete and edit records.
2. Screenshots of A5 main page including Data Table and error page when no data is entered.
3. Chapter Questions (Ch. 11, 12)

#### README.md file should include the following items:

* Screenshot of A5 Home;
* Screenshot of Error Page;
* Link to local lis4381 web app;

> Assignment screenshot will appear below.
> 
> Two screenshots are shown in this README file.
>

#### Assignment Screenshot:

*Screenshot of Data Table*:

![Data Table](img/capture1.PNG)

*Screenshot of Error Page*

![Failed Validation](img/capture2.PNG)

*Link to Local Web App*:

[Local Web App](http://localhost/lis4381/index.php)
