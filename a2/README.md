> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Carter Bass

### Assignment 2 Requirements:

*Two Parts:*

1. Backward-Engineer Bruschetta Recipe App using available resources.
2. Chapter Questions (Ch. 3 & Ch. 4)

#### README.md file should include the following items:

* Screenshot of running application's first user interface;
* Screenshot of running application's second user interface;

> Assignment screenshots will appear below.
> 
> Two separate screenshots are shown in this README file.
>

#### Assignment Screenshots:

*Screenshot of running application's first user interface*:

![Bruschetta Recipe 1st UI](img/recipe1.png)

*Screenshot of running application's second user interface*:

![Bruschetta Recipe 2nd UI](img/recipe2.PNG)
