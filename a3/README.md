# LIS4381 - Mobile Web Application Development

## Carter Bass

### Assignment 3 Requirements:

*Three Parts:*

1. Create a Pet Store ERD in MySQL and ensure it forward-engineers with 10 unique records.
2. Screenshot of ERD along with links to the MWB and SQL files.
3. Chapter Questions (Ch. 5 & 6)

#### README.md file should include the following items:

* Screenshot of Pet Store ERD;
* Link to MWB file;
* Link to SQL file;

> Assignment screenshot will appear below.
> 
> One screenshot is shown in this README file.
>

#### Assignment Screenshot:

*Screenshot of Pet Store ERD*:

![Pet Store ERD](img/a3.png)

*Link to MWB file*:

[Pet Store MWB File](docs/a3.mwb)

*Link to SQL file*:

[Pet Store SQL File](docs/a3.sql)
