# LIS4381 - Mobile Web Application Development

## Carter Bass

### Assignment Links

*Assignment 1*
[A1 Repository Link](a1/README.md "Assignment 1")

    - Configure Git and Bitbucket
    - Create LIS4381 Repository
    - Git commands
    - Install AMPPS, Java, and Android Studio
    - Screenshots of installed programs running
    - Ch. 1 and Ch. 2 questions

*Assignment 2*
[A2 Repository Link](a2/README.md "Assignment 2")

    - Backward-Engineer Healthy Recipe App
    - Screenshots of App running
    - Ch. 3 and Ch. 4 questions

*Assignment 3*
[A3 Repository Link](a3/README.md "Assignment 3")

    - Create ERD for a Pet Store database
    - Forward-Engineer with 10 unique records
    - PNG of the ERD
    - MWB file
    - SQL file

*Project 1*
[P1 Repository Link](p1/README.md "Project 1")

    - Backward-Engineer screenshots to make Business Card applications
    - Create launcher icon
    - Add background to both activities
    - Add border to around image and button
    - Add text shadow around button
    - Ch. 7 and Ch. 8 questions

*Assignment 4*
[A4 Repository Link](a4/README.md "Assignment 4")

    - Backward-Engineer screenshots to create web app
    - Test Failed validation on data
    - Test Passing validation on data
    - Link to local LISS4381 web app
    - Ch. 9, 10, and 19 questions

*Assignment 5*
[A5 Repository Link](a5/README.md "Assignment 5")

    - Create web app that communicates to a database
    - Allow user to Add, Delete, and Edit Records
    - Test Passing Validation
    - Test Failed Validation
    - Link to local LIS4381 web app
    - Ch. 11, 12 questions

*Project 2*
[P2 Repository Link](p2/README.md "Project 2")

    - Create web app that will edit and delete records in a database
    - Use server-side validation to test data
    - Create an RSS feed connected to local web app
    - Link to local LIS4381 web app
    - Ch. 13, 14 questions