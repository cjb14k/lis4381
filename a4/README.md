# LIS4381 - Mobile Web Application Development

## Carter Bass

### Assignment 4 Requirements:

*Three Parts:*

1. Use screenshots to backward engineer website to test data inputs
2. Screenshots of Main Page of website, failed validation, and passed validation.
3. Chapter Questions (Ch. 9, 10, 19)

#### README.md file should include the following items:

* Screenshot of Main Page;
* Screenshot of Failed Validation and Passed Validation;
* Link to loval lis4381 web app;

> Assignment screenshot will appear below.
> 
> One screenshot is shown in this README file.
>

#### Assignment Screenshot:

*Screenshot of Local Web App*:

![Web App Main Screen](img/img1.PNG)

*Screenshot of Failed Validation

![Failed Validation](img/img2.PNG)

*Screenshot of Passed Validation*

![Passed Validation](img/img3.PNG)

*Link to Local Web App*:

[Local Web App](http://localhost/lis4381/a4/index.php)
